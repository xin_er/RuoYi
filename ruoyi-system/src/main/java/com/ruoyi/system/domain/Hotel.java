package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 酒店对象 hotel
 * 
 * @author zss
 * @date 2019-09-06
 */
public class Hotel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 运营商ID */
    private Long companyId;

    /** 二级运营商ID */
    private Long operatorId;

    /** 经销商ID */
    private Long dealerId;

    /** 酒店编号 */
    private Long hotelNum;

    /** 酒店名称 */
    @Excel(name = "酒店名称")
    private String hotelName;

    /** 联系人名称 */
    @Excel(name = "联系人名称")
    private String linkMan;

    /** 手机号 */
    @Excel(name = "手机号")
    private String tel;

    /** 酒店LOGO */
    private String logo;

    /** 全局字幕 */
    private String comm;

    /** epg主栏目 */
    private String epgFirst;

    /** epg次栏目 */
    private String epgSecond;

    /** 热点名称 */
    private String wifi;

    /** 热点密码 */
    private String wifiPwd;

    /** 直播epg拉取地址 */
    private String epgHome;

    /** vod地址 */
    private String vod;

    /** 应用升级地址 */
    private String applicationUp;

    /** rom升级地址 */
    private String romUp;

    /** 省 */
    private String province;

    /** 市 */
    private String city;

    /** 区县 */
    private String area;

    /** 欢迎词 */
    private String welcome;

    /** 接口文档中的通知公告管理地址，现用于生成二维码的地址 */
    private String txtUrl;

    /** 酒店到期时间 */
    @Excel(name = "酒店到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date validDate;

    /** null */
    private String sspwdType;

    /** 已到期提示 */
    private String sTip;

    /** 即将到期提示 */
    private String nTip;

    /** 提前多久提示（天） */
    private Long tipTime;

    /** bom地址 */
    private String bom;

    /** 酒店编号 */
    private String mdNum;

    /** 背景图片 */
    private String bground;

    /** 酒店二维码 */
    private String qrcode;

    /** 删除标记 0 正常 1 删除 2 到期 */
    @Excel(name = "删除标记 0 正常 1 删除 2 到期")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCompanyId(Long companyId) 
    {
        this.companyId = companyId;
    }

    public Long getCompanyId() 
    {
        return companyId;
    }
    public void setOperatorId(Long operatorId) 
    {
        this.operatorId = operatorId;
    }

    public Long getOperatorId() 
    {
        return operatorId;
    }
    public void setDealerId(Long dealerId) 
    {
        this.dealerId = dealerId;
    }

    public Long getDealerId() 
    {
        return dealerId;
    }
    public void setHotelNum(Long hotelNum) 
    {
        this.hotelNum = hotelNum;
    }

    public Long getHotelNum() 
    {
        return hotelNum;
    }
    public void setHotelName(String hotelName) 
    {
        this.hotelName = hotelName;
    }

    public String getHotelName() 
    {
        return hotelName;
    }
    public void setLinkMan(String linkMan) 
    {
        this.linkMan = linkMan;
    }

    public String getLinkMan() 
    {
        return linkMan;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setLogo(String logo) 
    {
        this.logo = logo;
    }

    public String getLogo() 
    {
        return logo;
    }
    public void setComm(String comm) 
    {
        this.comm = comm;
    }

    public String getComm() 
    {
        return comm;
    }
    public void setEpgFirst(String epgFirst) 
    {
        this.epgFirst = epgFirst;
    }

    public String getEpgFirst() 
    {
        return epgFirst;
    }
    public void setEpgSecond(String epgSecond) 
    {
        this.epgSecond = epgSecond;
    }

    public String getEpgSecond() 
    {
        return epgSecond;
    }
    public void setWifi(String wifi) 
    {
        this.wifi = wifi;
    }

    public String getWifi() 
    {
        return wifi;
    }
    public void setWifiPwd(String wifiPwd) 
    {
        this.wifiPwd = wifiPwd;
    }

    public String getWifiPwd() 
    {
        return wifiPwd;
    }
    public void setEpgHome(String epgHome) 
    {
        this.epgHome = epgHome;
    }

    public String getEpgHome() 
    {
        return epgHome;
    }
    public void setVod(String vod) 
    {
        this.vod = vod;
    }

    public String getVod() 
    {
        return vod;
    }
    public void setApplicationUp(String applicationUp) 
    {
        this.applicationUp = applicationUp;
    }

    public String getApplicationUp() 
    {
        return applicationUp;
    }
    public void setRomUp(String romUp) 
    {
        this.romUp = romUp;
    }

    public String getRomUp() 
    {
        return romUp;
    }
    public void setProvince(String province) 
    {
        this.province = province;
    }

    public String getProvince() 
    {
        return province;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setArea(String area) 
    {
        this.area = area;
    }

    public String getArea() 
    {
        return area;
    }
    public void setWelcome(String welcome) 
    {
        this.welcome = welcome;
    }

    public String getWelcome() 
    {
        return welcome;
    }
    public void setTxtUrl(String txtUrl) 
    {
        this.txtUrl = txtUrl;
    }

    public String getTxtUrl() 
    {
        return txtUrl;
    }
    public void setValidDate(Date validDate) 
    {
        this.validDate = validDate;
    }

    public Date getValidDate() 
    {
        return validDate;
    }
    public void setSspwdType(String sspwdType) 
    {
        this.sspwdType = sspwdType;
    }

    public String getSspwdType() 
    {
        return sspwdType;
    }
    public void setSTip(String sTip) 
    {
        this.sTip = sTip;
    }

    public String getSTip() 
    {
        return sTip;
    }
    public void setNTip(String nTip) 
    {
        this.nTip = nTip;
    }

    public String getNTip() 
    {
        return nTip;
    }
    public void setTipTime(Long tipTime) 
    {
        this.tipTime = tipTime;
    }

    public Long getTipTime() 
    {
        return tipTime;
    }
    public void setBom(String bom) 
    {
        this.bom = bom;
    }

    public String getBom() 
    {
        return bom;
    }
    public void setMdNum(String mdNum) 
    {
        this.mdNum = mdNum;
    }

    public String getMdNum() 
    {
        return mdNum;
    }
    public void setBground(String bground) 
    {
        this.bground = bground;
    }

    public String getBground() 
    {
        return bground;
    }
    public void setQrcode(String qrcode) 
    {
        this.qrcode = qrcode;
    }

    public String getQrcode() 
    {
        return qrcode;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("companyId", getCompanyId())
            .append("operatorId", getOperatorId())
            .append("dealerId", getDealerId())
            .append("hotelNum", getHotelNum())
            .append("hotelName", getHotelName())
            .append("linkMan", getLinkMan())
            .append("tel", getTel())
            .append("logo", getLogo())
            .append("createTime", getCreateTime())
            .append("comm", getComm())
            .append("epgFirst", getEpgFirst())
            .append("epgSecond", getEpgSecond())
            .append("wifi", getWifi())
            .append("wifiPwd", getWifiPwd())
            .append("epgHome", getEpgHome())
            .append("vod", getVod())
            .append("applicationUp", getApplicationUp())
            .append("romUp", getRomUp())
            .append("province", getProvince())
            .append("city", getCity())
            .append("area", getArea())
            .append("welcome", getWelcome())
            .append("txtUrl", getTxtUrl())
            .append("validDate", getValidDate())
            .append("sspwdType", getSspwdType())
            .append("sTip", getSTip())
            .append("nTip", getNTip())
            .append("tipTime", getTipTime())
            .append("bom", getBom())
            .append("mdNum", getMdNum())
            .append("bground", getBground())
            .append("qrcode", getQrcode())
            .append("status", getStatus())
            .toString();
    }
}
