package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Hotel;
import java.util.List;

/**
 * 酒店Mapper接口
 * 
 * @author zss
 * @date 2019-09-06
 */
public interface HotelMapper 
{
    /**
     * 查询酒店
     * 
     * @param id 酒店ID
     * @return 酒店
     */
    public Hotel selectHotelById(Long id);

    /**
     * 查询酒店列表
     * 
     * @param hotel 酒店
     * @return 酒店集合
     */
    public List<Hotel> selectHotelList(Hotel hotel);

    /**
     * 新增酒店
     * 
     * @param hotel 酒店
     * @return 结果
     */
    public int insertHotel(Hotel hotel);

    /**
     * 修改酒店
     * 
     * @param hotel 酒店
     * @return 结果
     */
    public int updateHotel(Hotel hotel);

    /**
     * 删除酒店
     * 
     * @param id 酒店ID
     * @return 结果
     */
    public int deleteHotelById(Long id);

    /**
     * 批量删除酒店
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHotelByIds(String[] ids);
}
