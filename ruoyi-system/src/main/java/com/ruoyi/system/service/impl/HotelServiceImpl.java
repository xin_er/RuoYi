package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.HotelMapper;
import com.ruoyi.system.domain.Hotel;
import com.ruoyi.system.service.IHotelService;
import com.ruoyi.common.core.text.Convert;

/**
 * 酒店Service业务层处理
 * 
 * @author zss
 * @date 2019-09-06
 */
@Service
public class HotelServiceImpl implements IHotelService 
{
    @Autowired
    private HotelMapper hotelMapper;

    /**
     * 查询酒店
     * 
     * @param id 酒店ID
     * @return 酒店
     */
    @Override
    public Hotel selectHotelById(Long id)
    {
        return hotelMapper.selectHotelById(id);
    }

    /**
     * 查询酒店列表
     * 
     * @param hotel 酒店
     * @return 酒店
     */
    @Override
    public List<Hotel> selectHotelList(Hotel hotel)
    {
        return hotelMapper.selectHotelList(hotel);
    }

    /**
     * 新增酒店
     * 
     * @param hotel 酒店
     * @return 结果
     */
    @Override
    public int insertHotel(Hotel hotel)
    {
        hotel.setCreateTime(DateUtils.getNowDate());
        return hotelMapper.insertHotel(hotel);
    }

    /**
     * 修改酒店
     * 
     * @param hotel 酒店
     * @return 结果
     */
    @Override
    public int updateHotel(Hotel hotel)
    {
        hotel.setUpdateTime(DateUtils.getNowDate());
        return hotelMapper.updateHotel(hotel);
    }

    /**
     * 删除酒店对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHotelByIds(String ids)
    {
        return hotelMapper.deleteHotelByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除酒店信息
     * 
     * @param id 酒店ID
     * @return 结果
     */
    public int deleteHotelById(Long id)
    {
        return hotelMapper.deleteHotelById(id);
    }
}
