package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Hotel;
import com.ruoyi.system.service.IHotelService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 酒店Controller
 * 
 * @author zss
 * @date 2019-09-06
 */
@Controller
@RequestMapping("/system/hotel")
public class HotelController extends BaseController
{
    private String prefix = "hotel";

    @Autowired
    private IHotelService hotelService;

    @RequiresPermissions("system:hotel:view")
    @GetMapping()
    public String hotel()
    {
        return prefix + "/hotel";
    }

    /**
     * 查询酒店列表
     */
    @RequiresPermissions("system:hotel:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Hotel hotel)
    {
        startPage();
        List<Hotel> list = hotelService.selectHotelList(hotel);
        return getDataTable(list);
    }

    /**
     * 导出酒店列表
     */
    @RequiresPermissions("system:hotel:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Hotel hotel)
    {
        List<Hotel> list = hotelService.selectHotelList(hotel);
        ExcelUtil<Hotel> util = new ExcelUtil<Hotel>(Hotel.class);
        return util.exportExcel(list, "hotel");
    }

    /**
     * 新增酒店
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存酒店
     */
    @RequiresPermissions("system:hotel:add")
    @Log(title = "酒店", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Hotel hotel)
    {
        return toAjax(hotelService.insertHotel(hotel));
    }

    /**
     * 修改酒店
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Hotel hotel = hotelService.selectHotelById(id);
        mmap.put("hotel", hotel);
        return prefix + "/edit";
    }

    /**
     * 修改保存酒店
     */
    @RequiresPermissions("system:hotel:edit")
    @Log(title = "酒店", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Hotel hotel)
    {
        return toAjax(hotelService.updateHotel(hotel));
    }

    /**
     * 删除酒店
     */
    @RequiresPermissions("system:hotel:remove")
    @Log(title = "酒店", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hotelService.deleteHotelByIds(ids));
    }
}
